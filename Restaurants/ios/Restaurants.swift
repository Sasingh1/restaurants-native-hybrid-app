//
//  Restaurants.swift
//  Restaurants
//
//  Created by Sartaj Singh on 14/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

struct Restaurants {
  
  let name: String?
  let distance: String?
  let offer: String?
  let address: String?
  let imageName: String?
  
  init?(json: [String: Any]?) {
    guard let json = json else {return nil}
    name = json["name"] as? String ?? ""
    distance = json["distance"] as? String ?? ""
    offer = json["offer"] as? String ?? ""
    address = json["address"] as? String ?? ""
    imageName = json["image"] as? String ?? ""
  }
  
  init() {
    self.init(json: [:])!
  }
}
