//
//  RestaurantsListHandler.swift
//  Restaurants
//
//  Created by Sartaj Singh on 14/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

class RestaurantsListHandler {
  static let sharedInstance = RestaurantsListHandler()
  var dataRestaurants: [Restaurants] = []
  
  func getRestaurants(completion: @escaping () -> Void) {
    DispatchQueue.main.async {
      let bundle = Bundle(for: type(of: self))
      if let path = bundle.path(forResource: "restaurantsList", ofType: "json") {
        if let data = try? Data.init(contentsOf: URL.init(fileURLWithPath: path))
        {
          guard let list = self.parseRestaurantsListJson(data: data) else {return}
          for json in list {
            let result = Restaurants.init(json: json)
            self.dataRestaurants.append(result!)
          }
          completion()
        }
      }
    }
  }
  
  func parseRestaurantsListJson(data: Data) -> [[String: Any]]? {
    let list:  [[String: Any]]? = try? JSONSerialization.jsonObject(with: data, options: []) as!  [[String: Any]]
    return list
  }
  
  
}
