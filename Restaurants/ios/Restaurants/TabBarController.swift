//
//  TabBarController.swift
//  Restaurants
//
//  Created by Sartaj Singh on 13/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit
//import React

class TabBarController: UITabBarController {

  override func viewDidLoad() {
    viewControllers = setupViewControllers()
  }
  
  func setupViewControllers() -> [UIViewController] {
    let restaurantsViewController = UIStoryboard(name: "RestaurantsViewController", bundle: nil).instantiateViewController(withIdentifier: "RestaurantsViewController") as! RestaurantsViewController
    let rnViewController = createReactNativePortfolioViewController()
    
    return [rnViewController, restaurantsViewController]
  }
  
  func createReactNativePortfolioViewController() -> UIViewController {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let rootView = RCTRootView(bridge: delegate.bridge!, moduleName: "Restaurants", initialProperties: nil)
    
    let reactNativeViewController = UIViewController()
    reactNativeViewController.view = rootView
    
    let tabBarItem = UITabBarItem(title: "Locations", image: UIImage(named: "first"), selectedImage: nil)
    reactNativeViewController.tabBarItem = tabBarItem
    
    return reactNativeViewController
  }

}
