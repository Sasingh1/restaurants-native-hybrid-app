//
//  RestaurantCell.swift
//  Restaurants
//
//  Created by Sartaj Singh on 14/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

class RestaurantCell: UITableViewCell {

  @IBOutlet weak var restaurantImage: UIImageView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var distance: UILabel!
  @IBOutlet weak var offer: UILabel!
  @IBOutlet weak var address: UILabel!
  
  var restaurantsModel: Restaurants? {
    didSet {
      guard let data = restaurantsModel else {
        return
      }
      restaurantImage.image = UIImage(named:data.imageName!)
      name.text = data.name
      distance.text = "\(data.distance!) Km away"
      offer.text = data.offer
      address.text = data.address
    }
  }
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  

}
