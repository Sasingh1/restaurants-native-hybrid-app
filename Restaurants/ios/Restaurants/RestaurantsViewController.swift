//
//  RestaurantsViewController.swift
//  Restaurants
//
//  Created by Sartaj Singh on 13/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

class RestaurantsViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  
  var dataSource:[Restaurants] = [Restaurants]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setUpUI()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)
    self.setUpDataSource()
  }
  
  func setUpUI(){
    self.title = "Restaurants"
  }
  
  @IBAction func actionCancel(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
  }
  
  func setUpDataSource() {
    if RestaurantsListHandler.sharedInstance.dataRestaurants.count != 0 {
      self.dataSource =  RestaurantsListHandler.sharedInstance.dataRestaurants
      self.tableView.reloadData()
    }else{

      DispatchQueue.main.async {
        RestaurantsListHandler.sharedInstance.getRestaurants(completion: {
          DispatchQueue.main.async {
            self.dataSource = RestaurantsListHandler.sharedInstance.dataRestaurants
            self.tableView.reloadData()
          }
        })
      }
    }
  }
}

extension RestaurantsViewController:UITableViewDataSource{
  func numberOfSections(in tableView: UITableView) -> Int{
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    return dataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell", for: indexPath) as! RestaurantCell
    cell.restaurantsModel = dataSource[indexPath.row]
    return cell
  }
}
