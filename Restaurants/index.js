/**
 * @format
 */

import {AppRegistry} from 'react-native';
import ReactNativeTab from './app/ReactNativeTab';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ReactNativeTab);
