/**
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import RestaurantsMap from './Components/RestaurantsMap'

export default class Restaurants extends Component {

  state = {
    restaurantsPlaces: []
  };

  componentWillMount() {
    //dummy coordinates to show restaurant places
    const coords = [
      {
        "latitude": 41.8921794,
        "longitude": -87.6366551,
      },
       {
        "latitude": 41.894652,
        "longitude": -87.638362,
      },
       {
        "latitude": 41.89993,
        "longitude": -87.63443,
      },
       {
        "latitude": 41.8967844,
        "longitude": -87.63562809999999,
      },
       {
        "latitude": 41.8938449,
        "longitude": -87.6417762,
      },
     ]
      this.setState({
        restaurantsPlaces: coords
      });  
  }

  render() {   
    return (
      <View style={styles.container} >   
        <View style={{ marginBottom: 20 }}>
          <Text style={styles.welcome}>Restaurants</Text>        
        </View>    
        <RestaurantsMap  restaurantsPlaces={this.state.restaurantsPlaces} />
      </View>
    );
  }
}
AppRegistry.registerComponent('Restaurants', () => Restaurants);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});