import React from 'react'
import { View, StyleSheet } from 'react-native'
import MapView from 'react-native-maps'

const restaurantsMap = props => {
    const restaurantsMarkers = props.restaurantsPlaces.map(userPlace => (
        <MapView.Marker coordinate={userPlace} />
      ));

    return (
      <View style={styles.mapContainer}>
            <MapView
                style={styles.map}
                region={{
                latitude: 41.8921794,
                longitude: -87.6366551,
                latitudeDelta: 0.0622,
                longitudeDelta: 0.0421
                }}
                showsUserLocation={true}
        >
            {restaurantsMarkers}
        </MapView>
      </View>
    );
}

const styles = StyleSheet.create({
    mapContainer: {
        width: '100%',
        height: "100%",     
        marginTop: 20   
    },
    map: {
        width: '100%',
        height: '100%'
    }
})
export default restaurantsMap;